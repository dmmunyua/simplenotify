# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Organization, OrganizationUserProfile, Document, Transaction
# Register your models here.

class OrganizationAdmin(admin.ModelAdmin):
                list_display = ('organization_name', 'organization_type', 'organization_code', 'organization_shortcode', 'organization_krapin', 'organization_contactname', 'organization_contactnumber')

class OrganizationUserProfileAdmin(admin.ModelAdmin):
				list_display = ('user', 'organization', 'role')

#This will be deleted later---------------------------------------------------------------------------

class DocumentAdmin(admin.ModelAdmin):
				list_display = ('id','description', 'document', 'uploaded_at', 'processed', 'organization')

class TransactionAdmin(admin.ModelAdmin):
				list_display = ('id','request_id', 'pesalink_id', 'transaction_amount', 'transaction_time', 'sender_account_number', 'sender_bank', 'receiver_account_number', 'receiver_bank', 'sender_msisdn', 'transaction_narrative', 'sender_name','sender_email')



admin.site.register(Organization, OrganizationAdmin)
admin.site.register(OrganizationUserProfile, OrganizationUserProfileAdmin)
admin.site.register(Document, DocumentAdmin)
admin.site.register(Transaction, TransactionAdmin)

#This will be deleted later---------------------------------------------------------------------------

