# Create your tasks here
from __future__ import absolute_import, unicode_literals
from celery import shared_task
import requests
import json
from django.http import JsonResponse
import datetime
from rest_framework.parsers import JSONParser
import csv



@shared_task
def sendTransactionTask(Id, MSISDN, BusinessShortCode,InvoiceNumber, TransID, TransTime, TransAmount, BillRefNumber, FirstName, MiddleName, LastName):
	url = 'https://api.birs.co.ke/api/external/chase/inbound/notification/replay'
	data = {
	'Id': Id,
	'MSISDN': MSISDN,
	'BusinessShortCode': BusinessShortCode,
	'InvoiceNumber': InvoiceNumber,
	'TransID': TransID,
	'TransAmount': TransAmount,
	'TransTime': TransTime,
	'BillRefNumber': BillRefNumber,
	'ThirdPartyTransID': 'MANUAL',
	'KYCInfoList':[
		{
			"KYCName":"FirstName",
	        "KYCValue":FirstName
		},
		{
			"KYCName":"MiddleName",
	        "KYCValue":MiddleName
		},
		{
			"KYCName":"FirstName",
	        "KYCValue":LastName
		}

	]
	}
	headers = {'Content-Type': 'application/json'}
	response = requests.post(url, data=json.dumps(data), headers=headers)
	#Add If Statements for Error Handling

	response_data = json.loads(response.text)

	return "OK"



